function BaseGame() {
    this.element = document.getElementById('game');
    this.makePlace();
    this.element.appendChild(this.place.element);
}

BaseGame.prototype.newGame = function () {
    this.place.makeGamePlace();
};

BaseGame.prototype.makePlace = function () {
    this.place = new Place('./images/maze.png');
};

BaseGame.prototype.endGame = function () {
    this.place.clearGamePlace();
};