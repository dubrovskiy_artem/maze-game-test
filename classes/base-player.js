function BasePlayer() {
    this.init();
}

BasePlayer.prototype.init = function(){
    this.createElement();
};

BasePlayer.prototype.setX = function (x) {
    this.x = x;
    this.element.style.left = x + 'px';
};

BasePlayer.prototype.setY = function (y) {
    this.y = y;
    this.element.style.top = y + 'px';
};

BasePlayer.prototype.moveTo = function (x, y) {
    this.setX(x);
    this.setY(y);
};

BasePlayer.prototype.createElement = function () {
    this.element = document.createElement('div');
};

BasePlayer.prototype.remove = function () {
    this.element.remove();
};