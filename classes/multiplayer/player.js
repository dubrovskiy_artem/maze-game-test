function Player(player) {
    this.id = player.id;
    this.socketId = player.socketId;

    BasePlayer.apply(this, arguments);
}

Player.prototype = Object.create(SomePlayer.prototype);

Player.prototype.initElementAttributes = function () {
    SomePlayer.prototype.initElementAttributes.apply(this, arguments);
    this.element.style.zIndex = 100;
};

Player.prototype.initListeners = function () {
    var self = this;
    this.element.addEventListener("mouseup", function(e){
        this.style.width = 15 + 'px';
        this.style.height = 15 + 'px';
        self.active = false;
    });

    this.element.addEventListener("mousedown", function(e){
        this.style.width = 17 + 'px';
        this.style.height = 17 + 'px';
        self.active = true;
    });
};

Player.prototype.init = function () {
    SomePlayer.prototype.init.apply(this, arguments);
    this.initListeners();
};