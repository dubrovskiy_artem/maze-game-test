function Place(imagePath, socket) {
    this.socket = socket;
    BasePlace.apply(this, arguments);
}
Place.prototype = Object.create(BasePlace.prototype);

Place.prototype.init = function () {
    BasePlace.prototype.init.apply(this, arguments);
    this.initListeners();
};

Place.prototype.initListeners = function () {
    var self = this;
    this.element.addEventListener("mousemove", function(e){
        if(self.player && self.player.active){
            self.draw(self.player.color, e.layerX, e.layerY)
            self.player.setX(e.layerX - 5);
            self.player.setY(e.layerY - 5);
            self.socket.send({'event': 'user-move','player': self.player});
            // if(e.layerX > 252 && e.layerX < 273 && e.layerY < 5){
            //     self.player.element.style.width = 15 + 'px';
            //     self.player.element.style.height = 15 + 'px';
            //     self.player = undefined;
            //     alert('congratulation')
            // }
        }
    });
};