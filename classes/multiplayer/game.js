function Game(socket) {
    this.socket = socket;
    this.anotherPlayers = [];
    BaseGame.apply(this, arguments);
}

Game.prototype = Object.create(BaseGame.prototype);

Game.prototype.makePlace = function () {
    this.place = new Place('./images/maze.png', this.socket);
};

Game.prototype.makeNotification = function (msg) {
    var notificationsArea = document.getElementById('notifications');
    var notify = document.createElement('p');
    notify.textContent = msg;

    notificationsArea.appendChild(notify);

    setTimeout(function () {
        notificationsArea.removeChild(notify);
    }, 2000)
};

Game.prototype.endGame = function () {
    BaseGame.prototype.endGame.apply(this, arguments);
    if(this.player){
        this.player.remove();
        this.player = undefined;
        this.place.removePlayer();
    }
    for(var i = 0; i < this.anotherPlayers.length; i++){
        this.anotherPlayers[i].remove();
    }
    this.anotherPlayers = [];
};

Game.prototype.addPlayer = function (player) {
    if(!this.player){
        this.player = new Player(player);
        this.element.appendChild(this.player.element);
        this.place.addPlayer(this.player);
    }
};

Game.prototype.addAnotherPlayer = function (anotherPlayer) {
    var playerIndex = this.anotherPlayers.findIndex(function (item) {
        return item.id === anotherPlayer.id;
    });
    if(playerIndex === -1){
        var player = new SomePlayer(anotherPlayer);
        this.element.appendChild(player.element);
        player.moveTo(anotherPlayer.x, anotherPlayer.y);
        this.anotherPlayers.push(player);

        this.makeNotification('New user connected!');
    }
};

Game.prototype.moveAnotherPlayer = function (anotherPlayer) {
    var player = this.anotherPlayers.find(function (item) {
        return item.id === anotherPlayer.id;
    });
    if(player){
        player.moveTo(anotherPlayer.x, anotherPlayer.y);
        //this.place.draw(player.color, player.x, player.y)
    }
};

Game.prototype.disconnectAnotherPlayer = function (anotherPlayer) {
    var player = this.anotherPlayers.find(function (item) {
        return item.id === anotherPlayer.id;
    });
    if(player){
        player.remove();
        this.makeNotification('User disconnected!');
    }
};