function SomePlayer(player) {
    this.id = player.id;
    this.socketId = player.socketId;

    BasePlayer.apply(this, arguments);
}

SomePlayer.prototype = Object.create(BasePlayer.prototype);

SomePlayer.prototype.initElementAttributes = function () {
    this.element.id = this.id;
    this.color = '#' + this.element.id;
    this.element.style = 'background-color: ' + this.color;
    this.element.className = 'player';
};

SomePlayer.prototype.init = function () {
    BasePlayer.prototype.init.apply(this, arguments);
    this.initElementAttributes();
};