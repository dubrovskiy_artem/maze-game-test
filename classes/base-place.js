function BasePlace(imagePath) {
    this.imagePath = imagePath;
    this.img = new Image();
    this.img.src = this.imagePath;

    this.init();
}

BasePlace.prototype.init = function(){
    this.createElement();
};

BasePlace.prototype.draw = function (color, x, y) {
    this.ctx.fillStyle = color;
    this.ctx.fillRect(x, y, 3, 3);
};

BasePlace.prototype.addPlayer = function (player) {
    this.player = player;
};

BasePlace.prototype.removePlayer = function () {
    this.player = undefined;
};

BasePlace.prototype.makeGamePlace = function () {
    var self = this;
    self.img.onload = function() {
        self.ctx.drawImage(self.img, 0, 0, self.img.width, self.img.height, 0, 0, self.element.width, self.element.height);
    };
};

BasePlace.prototype.clearGamePlace = function () {
    this.ctx.clearRect(0, 0, this.element.width, this.element.height);
    this.ctx.drawImage(this.img, 0, 0, this.img.width, this.img.height, 0, 0, this.element.width, this.element.height);
};

BasePlace.prototype.createElement = function () {
    this.element = document.createElement('canvas');
    this.element.setAttribute('width', '500');
    this.element.setAttribute('height', '500');
    this.ctx = this.element.getContext("2d");
};

BasePlace.prototype.init = function(){
    this.createElement();
    this.makeGamePlace();
};