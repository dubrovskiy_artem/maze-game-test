function Player() {
    this.active = false;

    BasePlayer.apply(this, arguments);
}

Player.prototype = Object.create(BasePlayer.prototype);

Player.prototype.init = function () {
    BasePlayer.prototype.init.apply(this, arguments);
    this.initElementAttributes();
    this.initListeners();
};

Player.prototype.initElementAttributes = function () {
    this.element.id = this.getRandomStr();
    this.color = '#' + this.element.id;
    this.element.style = 'background-color: ' + this.color;
    this.element.className = 'player';
};

Player.prototype.getRandomStr = function() {
    var letters = '0123456789ABCDEF';
    var str = '';
    for (var i = 0; i < 6; i++) {
        str += letters[Math.floor(Math.random() * 16)];
    }
    return str;
};

Player.prototype.initListeners = function () {
    var self = this;
    this.element.addEventListener("mouseup", function(e){
        this.style.width = 15 + 'px';
        this.style.height = 15 + 'px';
        self.active = false;
    });

    this.element.addEventListener("mousedown", function(e){
        this.style.width = 17 + 'px';
        this.style.height = 17 + 'px';
        self.active = true;
    });
};