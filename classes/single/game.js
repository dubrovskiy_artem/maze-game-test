function Game() {
    BaseGame.apply(this, arguments);
}

Game.prototype = Object.create(BaseGame.prototype);

Game.prototype.newGame = function () {
    BaseGame.prototype.newGame.apply(this, arguments);
    if(!this.player){
        this.player = new Player();
        this.element.appendChild(this.player.element);
        this.place.addPlayer(this.player);
    }
};

Game.prototype.endGame = function () {
    BaseGame.prototype.endGame.apply(this, arguments);
    if(this.player){
        this.player.remove();
        this.player = undefined;
        this.place.removePlayer();
    }
};