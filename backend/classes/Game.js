var Player = require('./Player');

class Game {
    constructor(){
        this.players = [];
    }

    addPlayer(socket){
        var newPlayer = new Player(socket.id);
        socket.json.send({'event': 'self', 'player': newPlayer});
        for(var i = 0; i < this.players.length; i++){
            if(socket.id !== this.players[i].socketId){
                socket.json.send({'event': 'user-joined', 'player': this.players[i]});
            }
        }
        socket.broadcast.json.send({'event': 'user-joined', 'player': newPlayer});
        this.players.push(newPlayer);
    }

    movePlayer(socket, msg){
        var playerIndex = this.players.findIndex(function (item) {
            return item.socketId === socket.id;
        });
        this.players[playerIndex].setX(msg.player.x);
        this.players[playerIndex].setY(msg.player.y);
        socket.broadcast.json.send(msg);
    }

    disconnectPlayer(socket){
        var playerIndex = this.players.findIndex(function (item) {
            return item.socketId === socket.id;
        });
        socket.broadcast.json.send({'event': 'disconnect-user', 'player': this.players[playerIndex]});
        this.players.splice(playerIndex, 1);
    }

    newGame(io, msg){
        for(var i = 0; i < this.players.length; i++){
            io.sockets.connected[this.players[i].socketId].send(msg);
            io.sockets.connected[this.players[i].socketId].send({'event': 'self', 'player': this.players[i]});
            for(var j = 0; j < this.players.length; j++){
                if(this.players[i].socketId !== this.players[j].socketId){
                    this.players[j].x = null;
                    this.players[j].y = null;
                    io.sockets.connected[this.players[i].socketId].send({'event': 'user-joined', 'player': this.players[j]});
                }
            }
        }
    }

    endGame(socket, msg){
        socket.json.send(msg);
        socket.broadcast.json.send(msg);
    }
}

module.exports = Game;