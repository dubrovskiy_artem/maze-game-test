class Player {
    constructor(socketId){
        this.id = this.getRandomStr();
        this.socketId = socketId;
        this.x = null;
        this.y = null;
    }

    setX(x) {
        this.x = x;
    }

    setY(y) {
        this.y = y;
    }

     getRandomStr() {
        var letters = '0123456789ABCDEF';
        var str = '';
        for (var i = 0; i < 6; i++) {
            str += letters[Math.floor(Math.random() * 16)];
        }
        return str;
    };
}

module.exports = Player;