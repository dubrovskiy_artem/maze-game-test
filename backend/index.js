var Game = require('./classes/Game');

var game = new Game();

var io = require('socket.io').listen(8080);

io.sockets.on('connection', function (socket) {

    game.addPlayer(socket);

    socket.on('message', function (msg) {
        if(msg.event === 'user-move'){
            game.movePlayer(socket, msg);
        }else if(msg.event === 'new-game'){
            game.newGame(io, msg);
        }else if(msg.event === 'end-game'){
            game.endGame(socket, msg);
        }
    });

    socket.on('disconnect', function() {
        game.disconnectPlayer(socket);
    });
});