window.onload = function() {
    var game = new Game();

    document.getElementById('new-game').addEventListener('click', function () {
        game.endGame();
        game.newGame();
    });
    document.getElementById('end-game').addEventListener('click', function () {
        game.endGame();
    });
};
