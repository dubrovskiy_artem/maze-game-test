window.onload = function() {
    var socket = io.connect('http://127.0.0.1:8080');
    var game = new Game(socket);

    socket.on('connect', function () {
        socket.on('message', function (msg) {

            switch(msg.event) {
                case 'user-joined':
                    game.addAnotherPlayer(msg.player);
                    break;
                case 'self':
                    game.addPlayer(msg.player);
                    break;
                case 'user-move':
                    game.moveAnotherPlayer(msg.player);
                    break;
                case 'disconnect-user':
                    game.disconnectAnotherPlayer(msg.player);
                    break;
                case 'new-game':
                    game.endGame();
                    game.newGame();
                    break;
                case 'end-game':
                    game.endGame();
                    break;
                case 'notification':
                    console.log(msg.message);
                    break;
            }
        });
    }).on('disconnect', function () {
        game.endGame();
    });

    document.getElementById('new-game').addEventListener('click', function () {
        socket.send({'event': 'new-game'});
    });
    document.getElementById('end-game').addEventListener('click', function () {
        socket.send({'event': 'end-game'});
    });
};